## TODO

0. Inspect and understand the `it.unibo.jecho.JEcho` class
1. Gradlefy the JEcho project
2. Use Gradle to declare dependencies instead of tracking .jar files
3. Use Gradle's `run` task to launch `it.unibo.jecho.JEcho`
4. Use Gradle properties to set up the echo modality (`lowercase`, `uppercase`, `normal`) upon `run`